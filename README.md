# characte_management

#### ##介绍
**性格测试后台权限管理系统**

#### 软件架构
 ![测试](./image/a.png)

![后台管理](./image/b.png)
#### 安装要求

1.  IDEA2018以上即可
2.  mysql5.7及以上
3.  数据库在resources目录下

#### 使用说明

1.  项目使用springboot+mybaitsplus+layui+shiro框架
2.  导入数据库，然后用idea打开项目，导入一下maven依赖即可使用
3.  发送邮件功能需要修改配置文件，暂时使用的是我个人的

#### 特技

1.  使用html转pdf进行下载
2.  发送邮件功能
3.  在线演示地址：（等上传服务器）
4.  mybaits-plus使用，shiro安全框架
